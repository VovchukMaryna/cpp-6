#include <iostream>

using namespace std;

int main()
{
	int number;
	
	cout << "Enter number: \t";
	cin >> number;
	
	cout << "\t\t" << number % 10 << (number / 10) % 10 << (number / 100) % 10 << (number / 1000) % 10 << endl;
	
	return 0;
}
